==========
flufl.enum
==========

A Python enumeration package.

The ``flufl.enum`` library is a Python enumeration package.  Its goal is to
provide simple, specific, concise semantics in an easy to read and write
syntax.  ``flufl.enum`` has just enough of the features needed to make
enumerations useful, but without a lot of extra baggage to weigh them down.
This work grew out of the Mailman 3.0 project.

Since enums were added to Python in 3.4, why use this package instead of the
Python standard library `enum <https://docs.python.org/3/library/enum.html>`_
package?  ``flufl.enum`` is intentionally simpler, and thus potentially faster
and easier to maintain.


Author
======

``flufl.enum`` is Copyright (C) 2004-2024 Barry Warsaw <barry@python.org>

Licensed under the terms of the Apache License Version 2.0.  See the LICENSE
file for details.


Project details
===============

 * Project home: https://gitlab.com/warsaw/flufl.enum
 * Report bugs at: https://gitlab.com/warsaw/flufl.enum/issues
 * Code hosting: https://gitlab.com/warsaw/flufl.enum.git
 * Documentation: http://fluflenum.readthedocs.org/
 * PyPI: https://pypi.python.org/pypi/flufl.enum
